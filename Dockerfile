FROM microsoft/aspnetcore-build:1.0-2.0 as build

WORKDIR /code

COPY . .

RUN dotnet restore

RUN dotnet publish --output /publish --configuration Release


FROM microsoft/aspnetcore:2.0

COPY --from=build /publish /app

WORKDIR /app

ENTRYPOINT [ "dotnet", "test.dll" ]